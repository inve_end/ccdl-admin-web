## Project setup

```bash
yarn install
```

### Compiles and hot-reloads for development

```bash
yarn serve
```

### Compiles and minifies for production

```bash
yarn build
```

### Lints and fixes files

```bash
yarn lint
```

### Run your unit tests

```bash
yarn test:unit
```

### Run your end-to-end tests

```bash
yarn test:e2e
```

### Run all tests

```bash
yarn test
```
