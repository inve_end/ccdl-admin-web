module.exports = {
  productionSourceMap: false,
  devServer: {
    proxy: {
      // '/':{
      //   target: 'http://18.136.80.153:9167',
      //   changeOrigin: true,
      //   pathRewrite: {
      //     '^/': '/'
      //   }
      // },
      '/market':{target:'http://18.136.80.153:9167'},
      '/admin':{target:'http://18.136.80.153:9167'}
    }
  },
  pwa: {
    name: 'vue-typescript-admin-template'
  }
}
