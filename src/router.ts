import Vue from 'vue';
import Router from 'vue-router';
import Layout from '@/views/layout/Layout.vue';

Vue.use(Router);

/*
  redirect:                      if `redirect: noredirect`, it won't redirect if click on the breadcrumb
  meta: {
    title: 'title'               the name showed in subMenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon showed in the sidebar
    breadcrumb: false            if false, the item will be hidden in breadcrumb (default is true)
    hidden: true                 if true, this route will not show in the sidebar (default is false)
  }
*/

export default new Router({
  // mode: 'history',  // Disabled due to Github Pages doesn't support this, enable this if you need.
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  },
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      component: () => import(/* webpackChunkName: "login" */ '@/views/login/index.vue'),
      meta: { hidden: true },
    },
    {
      path: '/404',
      component: () => import(/* webpackChunkName: "404" */ '@/views/404.vue') ,
      meta: { hidden: true },
    },
    {
      path: '/',
      component: Layout,
      redirect: '/dashboard',
      name: 'Dashboard',
      meta: { hidden: true },
      children: [{
        path: 'dashboard',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/index.vue'),
      }],
    }, {
      path: '/operation',
      component: Layout,
      redirect: '/operation/statistics',
      name: 'operation',
      meta: { title: '运营相关', icon: 'example', prem: 'market'},
      children: [
        {
          path: 'statistics',
          name: 'statistics',
          component: () => import('@/views/statistMag/index.vue'),
          meta: { title: '统计管理', icon: 'yonghuguanli', prem: 'market_stats'}
        }, {
          path: 'group',
          name: 'group',
          component: () => import('@/views/leaderMag/index.vue'),
          meta: { title: '组长管理', icon: 'yonghuguanli', prem: 'market_leader_get'}
        }, {
          path: 'staff',
          name: 'staff',
          component: () => import('@/views/staffMag/index.vue'),
          meta: { title: '组员管理', icon: 'yonghuguanli', prem: 'market_staff_get'}
        },{
          path: 'chn',
          name: 'chn',
          component: () => import('@/views/chnList/index.vue'),
          meta: { title: '渠道管理', icon: 'yonghuguanli', prem: 'market_channel'}
        }, {
          path: 'operationuser',
          name: 'operationuser',
          component: () => import('@/views/user/index.vue'),
          meta: { title: '用户信息', icon: 'yonghuguanli', prem: 'market_user_info'}
        }, {
          path: 'bind',
          name: 'bind',
          component: () => import('@/views/bind/index.vue'),
          meta: { title: '绑定管理', icon: 'yonghuguanli', prem: 'market_list_bind_record'}
        }
        , {
          path: 'dailyData',
          name: 'dailyData',
          component: () => import('@/views/dailyData/index.vue'),
          meta: { title: '每日数据', icon: 'yonghuguanli', prem: 'daily_channel_report'}
        },
         {
          path: 'configData',
          name: 'configData',
          component: () => import('@/views/configData/index.vue'),
          meta: { title: '数据配置', icon: 'yonghuguanli', prem: 'market_stats_adjustment'}
        },
      ],
    }, {
      path: '/auth',
      component: Layout,
      redirect: '/auth/user',
      name: 'auth',
      meta: { title: '权限管理', icon: 'example', prem: 'admin_group'},
      children: [
        {
          path: 'user',
          name: 'user',
          component: () => import('@/views/userMag/index.vue'),
          meta: { title: '用户管理', icon: 'yonghuguanli', prem: 'admin_user_group'}
        }, {
          path: 'role',
          name: 'role',
          component: () => import('@/views/roleMag/index.vue'),
          meta: { title: '角色管理', icon: 'yonghuguanli', prem: 'admin_role_group'}
        }
      ]
    },
    {
      path: '*',
      redirect: '/404',
      meta: { hidden: true },
    },
  ],
});
