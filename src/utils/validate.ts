export const isValidUsername = (str: string) => str.replace(/^\s+|\s+$/g,'').length >= 0;

export const isExternal = (path: string) => /^(https?:|mailto:|tel:)/.test(path);
