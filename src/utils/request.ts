import axios from 'axios';
import { Message, MessageBox } from 'element-ui';
import { removeUserId } from '@/utils/auth';
import { UserModule } from '@/store/modules/user';
let url = ''
if (process.env.NODE_ENV === 'development'){
  url = 'http://18.136.80.153:9167'
} else if (process.env.NODE_ENV === 'production'){
  url = 'http://qiko69.com:9167'
}


const service = axios.create({
  baseURL: url,
  timeout: 400000,
  withCredentials: true,
});

// Request interceptors
service.interceptors.request.use(
  (config) => {
    // Add X-Token header to every request, you can add other custom headers here
    // if (UserModule.token) {
    //   config.headers['X-Token'] = getToken();
    // }
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

// Response interceptors
service.interceptors.response.use(
  response => {
    /**
     * status为非0是抛错 可结合自己业务进行修改
     */
    return response.data
  },
  error => {
    // console.log('err' + error)// for debug
    if(error.response.data.msg){
      Message({
        message: error.response.data.msg,
        type: 'error'
      })
    } else {
      Message({
        message: error.message,
        type: 'error',
        duration: 5 * 1000
      })
    }
    if(error.response.data.status === 1005){
      // debugger
      removeUserId()
      window.location.replace('/#/login')
    }
    return Promise.reject(error)
  }
);

export default service;
