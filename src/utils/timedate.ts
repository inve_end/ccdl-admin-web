
const dateFormat = require('dateformat')
const defaultTime: string = (new Date()).toDateString()
const dateToday = dateFormat(defaultTime, 'yyyy-mm-dd')
const dateYesterday = dateFormat(new Date(Date.parse(defaultTime) - 1000 * 60 * 60 * 24), 'yyyy-mm-dd')
const dateThreeday = dateFormat(new Date(Date.parse(defaultTime) - 1000 * 60 * 60 * 24 * 3), 'yyyy-mm-dd')
const dateWeek = dateFormat(new Date(Date.parse(defaultTime) - 1000 * 60 * 60 * 24 * 7), 'yyyy-mm-dd')

export default {
  dateToday,
  dateYesterday,
  dateThreeday,
  dateWeek
}
