import Cookies from 'js-cookie';

const TokenKey = 'ccdl_admin_token';
const IDKey = 'ccdl_admin_userid';

export const getToken = () => Cookies.get(TokenKey);

export const setToken = (token: string) => Cookies.set(TokenKey, token);

export const removeToken = () => Cookies.remove(TokenKey);

export const getUserId = () => Cookies.get(IDKey);

export const setUserId = (id: any) => Cookies.set(IDKey, id);

export const removeUserId = () => Cookies.remove(IDKey);
