import request from '@/utils/request';

export const getList = (params: any) =>
  request({
    url: '/table/list',
    method: 'get',
    params,
  });

export function getUserList(params: any) {
  return request({
    url: '/admin/users/',
    method: 'get',
    params
  })
}

export function getGameUserList(data: object) {
  return request({
    url: '/market/game_user/user_info/',
    method: 'post',
    data
  })
}

export function getBindList(params: object) {
  return request({
    url: '/market/game_user/channel/bind/',
    method: 'get',
    params
  })
}
export function getBindUserList(params: object) {
  return request({
    url: '/market/game_user/channel/',
    method: 'get',
    params
  })
}

export function saveAddBindUser(data: object) {
  return request({
    url: `/market/game_user/channel/bind/`,
    method: 'post',
    data
  })
}
export function deleteUser(id: number) {
  return request({
    url: `/admin/users/${id}/`,
    method: 'delete'
  })
}

export function getUserDetail(id: number) {
  return request({
    url: `/admin/users/${id}/`,
    method: 'get'
  })
}

export function editUserDetail(data: object,id: number) {
  return request({
    url: `/admin/users/${id}/`,
    method: 'put',
    data
  })
}

export function saveAddUser(data: object) {
  return request({
    url: `/admin/users/`,
    method: 'post',
    data
  })
}

export function getRoleList(params: any) {
  return request({
    url: '/admin/roles/',
    method: 'get',
    params
  })
}

export function getRoleDetail(id: number) {
  return request({
    url: `/admin/roles/${id}/`,
    method: 'get'
  })
}

export function getPermissions() {
  return request({
    url: `/admin/permissions/`,
    method: 'get'
  })
}


export function editRoleDetail(data: object,id: number) {
  return request({
    url: `/admin/roles/${id}/`,
    method: 'put',
    data
  })
}

export function saveAddRole(data:object) {
  return request({
    url: `/admin/roles/`,
    method: 'post',
    data
  })
}

export function getStatistList(params: any) {
  return request({
    url: '/admin/roles/',
    method: 'get',
    params
  })
}

export function getchnList(params: any) {
  return request({
    url: '/market/channel/',
    method: 'get',
    params
  })
}

export function deletechn(id: number) {
  return request({
    url: `/market/channel/${id}/`,
    method: 'delete'
  })
}
export function statuschn(id: number) {
    return request({
        url: `/market/channel/${id}/`,
        method: 'get'
    })
}

export function updatechn(data: object,id: number) {
  return request({
    url: `/market/channel/${id}/`,
    method: 'put',
    data
  })
}

export function saveAddchn(data: object) {
  return request({
    url: `/market/channel/`,
    method: 'post',
    data
  })
}

export function getUserInfoList(params: any) {
  return request({
    url: '/market/user_info/',
    method: 'get',
    params
  })
}

export function deleteUserInfo(id: number) {
  return request({
    url: `/market/user_info/${id}/`,
    method: 'delete'
  })
}

export function updateUserInfo(data: object,id: number) {
  return request({
    url: `/market/user_info/${id}/`,
    method: 'put',
    data
  })
}

export function saveAddUserInfo(data: object) {
  return request({
    url: `/market/user_info/`,
    method: 'post',
    data
  })
}

export function getStatistReport(data: any) {
  return request({
    url: `/market/report/channel_v2/`,
    method: 'post',
    data
  })
}

export function getChnOptions(params: any) {
  return request({
    url: '/market/select_options/',
    method: 'get',
    params
  })
}
export function getOperatorOptions(params: any) {
  return request({
    url: '/market/game_user/channel/bind/select_options/',
    method: 'get',
    params
  })
}
export function getIosChnOptions() {
  return request({
    url: '/market/select_options/channel/?only_ios_channel=true',
    method: 'get',
  })
}


export function getDailyDataList(data: object) {
  return request({
    url: '/market/report/daily_channel/',
    method: 'post',
    data
  })
}

export function getConfigInfoList(params: any) {
  return request({
    url: '/market/report/adjustment/',
    method: 'get',
    params
  })
}

export function getadjustmentInfoList(params: any) {
  return request({
    url: '/market/report/adjustment/preset/',
    method: 'get',
    params
  })
}
export function saveAddConfigInfo(data: object) {
  return request({
    url: `/market/report/adjustment/`,
    method: 'post',
    data
  })
}

export function updateConfigInfo(data: object) {
  return request({
    url: `/market/report/adjustment/`,
    method: 'put',
    data
  })
}
export function deleteConfigInfo(data: object) {
  return request({
    url: `/market/report/adjustment/`,
    method: 'delete',
    data
  })
}
