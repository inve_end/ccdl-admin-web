import request from '@/utils/request';

export const login = (name: string, password: string) =>
  request({
    url: '/admin/login/',
    method: 'post',
    data: {
      name,
      password,
    },
  });

export const getInfo = (id: any) =>
  request({
    url: `/admin/user_info/`,
    method: 'get',
  });

export const logout = () =>
  request({
    url: '/user/logout/',
    method: 'post',
  });
